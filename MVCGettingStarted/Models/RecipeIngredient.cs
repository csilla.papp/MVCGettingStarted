﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCGettingStarted.Models
{
    public class RecipeIngredient
    {        
        public double Quantity { get; set; }

        [Key, Column(Order = 0)]
        public Guid IngredientUid { get; set; }
        public virtual Ingredient Ingredient { get; set; }

        [Key, Column(Order = 1)]
        public Guid RecipeUid { get; set; }
        public virtual Recipe Recipe { get; set; }
    }
}