﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCGettingStarted.Models
{
    public class Step
    {
        [Key, Column(Order = 0)]
        public Guid RecipeUid { get; set; }
        public virtual Recipe Recipe { get; set; }

        [Key, Column(Order = 1)]
        public string Content { get; set; }

        public int Order { get; set; }
    }
}