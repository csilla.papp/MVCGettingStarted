﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MVCGettingStarted.Models
{
    public class RecipeObject
    {
        [Key]
        public Guid Uid { get; set; }

        [Required]
        public string Name { get; set; }
    }
}