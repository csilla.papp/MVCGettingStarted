﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCGettingStarted.Models;

namespace MVCGettingStarted.Controllers
{
    public class IngredientsController : Controller
    {
        private MyRecipesContext db = new MyRecipesContext();

        // GET: Ingredients
        public ActionResult Index()
        {
            var ingredients = db.Ingredients.Include(i => i.MeasurementUnit);
            return View(ingredients.ToList());
        }

        // GET: Ingredients/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ingredient ingredient = db.Ingredients.Find(id);
            if (ingredient == null)
            {
                return HttpNotFound();
            }
            return View(ingredient);
        }

        // GET: Ingredients/Create
        public ActionResult Create()
        {
            ViewBag.MeasurementUnits = new SelectList(db.MeasurementUnits, "Uid", "Name");
            return PartialView("_Create");
        }

        // POST: Ingredients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MeasurementUnitUid,Name,Price")] CreateIngredientViewModel ingredientViewModel)
        {
            if (ModelState.IsValid)
            {
                Ingredient ingredient = new Ingredient
                {
                    Uid = Guid.NewGuid(),
                    Name = ingredientViewModel.Name,
                    MeasurementUnitUid = ingredientViewModel.MeasurementUnitUid,
                    Price = ingredientViewModel.Price
                };

                db.Ingredients.Add(ingredient);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.MeasurementUnits = new SelectList(db.MeasurementUnits, "Uid", "Name", ingredientViewModel.MeasurementUnitUid);
            return PartialView("_Create", ingredientViewModel);
        }

        // GET: Ingredients/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ingredient ingredient = db.Ingredients.Find(id);
            if (ingredient == null)
            {
                return HttpNotFound();
            }
            ViewBag.MeasurementUnits = new SelectList(db.MeasurementUnits, "Uid", "Name", ingredient.MeasurementUnitUid);
            return View(ingredient);
        }

        // POST: Ingredients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Uid,MeasurementUnitUid,Name,Price")] Ingredient ingredient)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ingredient).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MeasurementUnits = new SelectList(db.MeasurementUnits, "Uid", "Name", ingredient.MeasurementUnitUid);
            return View(ingredient);
        }

        // GET: Ingredients/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ingredient ingredient = db.Ingredients.Find(id);
            if (ingredient == null)
            {
                return HttpNotFound();
            }
            return View(ingredient);
        }

        // POST: Ingredients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Ingredient ingredient = db.Ingredients.Find(id);
            db.Ingredients.Remove(ingredient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
